var express = require('express');
var bodyParser = require('body-parser')
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var db = require('./db')
var artistsController = require('./contollers/artists');
var drugController = require('./contollers/drug');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true,
}));


app.get('/drug', drugController.all);
app.post('/drug', drugController.create);
app.get('/drug/:name', drugController.find);

db.connect('mongodb://localhost:27017', function(err, client) {
    if (err) {
        return console.log(err);
    }

    app.listen(3000, function() {
        console.log("сервер начал свою работу");
    })
})