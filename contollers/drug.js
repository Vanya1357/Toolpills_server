var Drug = require('../models/drug');

exports.all = function(req, res) {
    Drug.all(function(err, docs) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.send(docs);
    })
}

exports.create = function(req, res) {
    var drug = {
        name: req.body.name,
        price: req.body.price,
        rating: req.body.rating,
    }
    Drug.create(drug, function(err, docs) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.sendStatus(200);
    })
}

exports.find = function(req, res) {
    Drug.find(req.params.name, function(err, doc) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.send(doc)
    })
}