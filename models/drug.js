var db = require('../db');
var ObjectID = require('mongodb').ObjectID;

exports.all = function(cb) {
    db.get().collection('druges').find().toArray(function(err, result) {
        cb(err, result);
    })
}

exports.create = function(drug, cb) {
    db.get().collection('druges').insert(drug, function(err, result) {
        cb(err, result);
    })
}

/*exports.find = function(name, cb) {
    db.get().collection('druges').findOne({ name: name}, function(err, result) {
        cb(err, result);
    })
}*/
exports.find = function(name, cb) {
    db.get().collection('druges').find({ name: name}).toArray(function(err, docs) {
        cb(err, docs);
    })
}