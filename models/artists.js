var db = require('../db');
var ObjectID = require('mongodb').ObjectID;

exports.all = function(cb) {
    db.get().collection('druges').find().toArray(function (err, docs) {
        cb(err, docs);
    })
}

exports.findById = function(id, cb) {
    db.get().collection('druges').findOne({ _id: ObjectID(id) }, function(err, doc) {
        cb(err, doc);
    })
}

exports.create = function(name, cb) {
    db.get().collection('druges').insert(name, function(err, doc) {
        cb(err, doc);
    })
}

exports.update = function(id, name, cb) {
    db.get().collection('druges').updateOne(
        { _id: ObjectID(id)},
        { $set: name},
        function(err, result) {
            cb(err, result);
        }
    )
}

exports.delete = function(id, cb) {
    db.get().collection('druges').deleteOne(
        { _id: ObjectID(id)},
        function(err, result) {
            cb(err, result);
        }
    )
}